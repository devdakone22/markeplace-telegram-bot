from settings.proxy import PROXY_FILE
from random import shuffle

with open(PROXY_FILE, 'r') as f:
    proxy_list = list(map(lambda x: x, f.readlines()))
shuffle(proxy_list)


def get_proxy_generator():
    while True:
        for proxy in proxy_list:
            yield proxy.strip()

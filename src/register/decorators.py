from functools import wraps

from src.register.errors import AlreadyRegisteredException
from src.register.variables import HANDLERS

COMMAND_HANDLERS = HANDLERS['COMMAND_HANDLERS']
MESSAGE_HANDLERS = HANDLERS['MESSAGE_HANDLERS']


def protect(logging: bool = False, error_file=None):
    def decorator(func: callable):
        from traceback import format_exc
        from time import strftime

        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception:
                if logging:
                    print(f"[{strftime('%x %X')}] Critical error intercepted:", file=error_file)
                    print(format_exc(), file=error_file)

        return wrapper

    return decorator


def command_handler(command_string: str, description: str = None):
    def decorator(func: callable):
        if command_string in COMMAND_HANDLERS.keys():
            raise AlreadyRegisteredException(
                f'Command Handler with command string {command_string} already registered!\n'
                f'Func {COMMAND_HANDLERS[command_string]} with name {COMMAND_HANDLERS[command_string].__name__}.'
            )

        COMMAND_HANDLERS[command_string] = func

        if description:
            func.__command_description = description
        elif func.__doc__:
            func.__command_description = func.__doc__
        else:
            func.__command_description = None

        return func

    return decorator


def message_handler(filter):
    def decorator(func: callable):
        # if MESSAGE_HANDLERS[filter]:
        #     raise AlreadyRegisteredException(
        #         f'Messsage Handler with filter {filter} already registered!\n'
        #         f'Func {MESSAGE_HANDLERS[filter]} with name {MESSAGE_HANDLERS[filter].__name__}.'
        #     )

        MESSAGE_HANDLERS.append((filter, func))

        return func

    return decorator


def error_handler(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if HANDLERS["ERROR_HANDLER"]:
            raise AlreadyRegisteredException(
                f'Error Handler already registered!\n'
                f'Func {HANDLERS["ERROR_HANDLER"]} with name {HANDLERS["ERROR_HANDLER"].__name__}.'
            )

        HANDLERS["ERROR_HANDLER"] = func

        return func(*args, **kwargs)

    return wrapper


def callback_query_handler(pattern):
    def decorator(func: callable):
        if pattern is None:  # register global callback handler
            if HANDLERS["CALLBACK_QUERY_HANDLERS"]:
                raise AlreadyRegisteredException(
                    f'Global CallBack Query Handler or some others CallBack Query Handlers already registered!'
                )

            HANDLERS["CALLBACK_QUERY_HANDLERS"] = func

        else:
            CQH = HANDLERS["CALLBACK_QUERY_HANDLERS"]
            if not isinstance(CQH, dict):
                raise AlreadyRegisteredException(
                    f'Global CallBack Query Handler already registered!'
                )

            HANDLERS["CALLBACK_QUERY_HANDLERS"][pattern] = func

        return func

    return decorator
